package packageg30126.neag.bogdan.ex5;

import java.awt.Color;



public class Pyramid2 extends Rectangle{
	
	public Pyramid2(Color color, int x, int y, int length, int width, String id, boolean fill) {
		super(color, x, y, length, width, id, fill);
		// TODO Auto-generated constructor stub
	}
	private static final int nr = 5;
	private static final int width = 32;
	private static final int height = 15;
	
	public static void createPyramid() {
		
		DrawingBoard b1 = new DrawingBoard();
		int i,j,c=0,k=nr;
		
		for(i=0;i<k;i++) {			
			for(j=i;j>0;j--) {
				int x = 100+height*(j+1)+(k-i)*height/2;
				int y = 300-width*(k-i+1);
				Shape brick = new Rectangle(Color.BLUE,x,y,height,width,"c",false);
				b1.addShape(brick);
				c++;
				
			}
			
		}
	}
	public static void main(String[] args) {
		createPyramid();
	}
}