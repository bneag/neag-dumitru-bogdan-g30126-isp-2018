package packageg30126.neag.bogdan.ex1;
import java.awt.*;

public class Circle extends Shape{

    private int radius;

    public Circle(Color color, int x,int y,int radius,String id,boolean fill) {
        super(color,x,y,id,fill);
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        if(getFill()==false)
        g.drawOval(getx(),gety(),radius,radius);
        else
        	g.fillOval(getx(), gety(), radius, radius);
    }
  
}
