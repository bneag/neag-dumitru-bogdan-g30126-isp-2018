package packageg30126.neag.bogdan.ex1;

import java.awt.*;

public abstract class Shape {

    private Color color;
    private int x,y;
    private String id;
    private boolean fill;
    
    public Shape(Color color,int x,int y,String id,boolean fill) {
        this.color = color;
        this.x=x;
        this.y=y;
        this.id=id;
        this.fill=fill;
    }

    public Color getColor() {
        return color;
    }
    public int getx(){
    	return x;
    }
    public int gety(){
    	return y;
    }
    public String getID(){
    	return id;
    }

    public void setColor(Color color) {
        this.color = color;
    }
    public boolean getFill(){
    	return fill;
    }

    public abstract void draw(Graphics g);
  
}
