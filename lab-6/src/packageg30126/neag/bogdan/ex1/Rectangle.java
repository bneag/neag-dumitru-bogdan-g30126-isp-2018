package packageg30126.neag.bogdan.ex1;

import java.awt.*;

public class Rectangle extends Shape{

    private int length,width;

    public Rectangle(Color color,int x,int y, int length,int width,String id,boolean fill) {
        super(color,x,y,id,fill);
        this.length = length;
        this.width=width;
        
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        g.setColor(getColor());
        if(getFill()==false)
        g.drawRect(getx(),gety(),length,width);
        else g.fillRect(getx(), gety(), length, width);
    }
   
}