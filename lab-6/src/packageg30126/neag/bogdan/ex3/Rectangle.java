package packageg30126.neag.bogdan.ex3;

	import java.awt.Color;
import java.awt.Graphics;



	public class Rectangle implements Shape{

	    private int length,width;
	    private Color color;
	    private int x,y;
	    private boolean fill;
	    private String id;
	    public Rectangle(Color color,int x,int y, int length,int width,String id,boolean fill) {
	        this.color=color;
	        this.x=x;
	        this.y=y;
	        this.length = length;
	        this.width=width;
	        this.id=id;
	        this.fill=fill;
	    }
	    public Color getColor() {
	        return color;
	    }
	    public int getx(){
	    	return x;
	    }
	    public int gety(){
	    	return y;
	    }
	    public String getID(){
	    	return id;
	    }

	    public void setColor(Color color) {
	        this.color = color;
	    }
	    public boolean getFill(){
	    	return fill;
	    }

	    @Override
	    public void draw(Graphics g) {
	        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
	        g.setColor(getColor());
	        if(getFill()==false)
	        g.drawRect(getx(),gety(),length,width);
	        else g.fillRect(getx(), gety(), length, width);
	    }
	   
	}

