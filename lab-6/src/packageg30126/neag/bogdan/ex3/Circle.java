package packageg30126.neag.bogdan.ex3;

import java.awt.Color;
import java.awt.Graphics;


public class Circle implements Shape{

    private int radius;
    private Color color;
    private int x,y;
    private String id;
    private boolean fill;
   
    public Circle(Color color, int x,int y,int radius,String id,boolean fill) {
       this.color=color;
       this.x=x;
       this.y=y;
       this.radius = radius;
       this.id=id;
       this.fill=fill;
    }

    public int getRadius() {
        return radius;
    }
    public Color getColor() {
        return color;
    }
    public int getx(){
    	return x;
    }
    public int gety(){
    	return y;
    }
    public String getID(){
    	return id;
    }

    public void setColor(Color color) {
        this.color = color;
    }
    public boolean getFill(){
    	return fill;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        if(getFill()==false)
        g.drawOval(getx(),gety(),radius,radius);
        else
        	g.fillOval(getx(), gety(), radius, radius);
    }
  
}
