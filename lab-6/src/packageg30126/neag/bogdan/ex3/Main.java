package packageg30126.neag.bogdan.ex3;
import java.awt.*;

public class Main {
    public static void main(String[] args) {
    	int x=10,y=58;
    	int z=100,w=80;
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.RED, x,y,90,"C1",false);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN, x,y,110,"C2",false);
        b1.addShape(s2);
        Shape s3 = new Rectangle(Color.BLUE,z,w,100,50,"R1",false);
        b1.addShape(s3);
        b1.deleteById("C1");
        Shape s4 = new Rectangle(Color.RED,z,w,170,200,"R2",true);
        b1.addShape(s4);
        Shape s5 = new Circle(Color.BLUE,z,w,20,"C3",true);
        b1.addShape(s5);
    }
}
