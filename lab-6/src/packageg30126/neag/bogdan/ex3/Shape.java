package packageg30126.neag.bogdan.ex3;

import java.awt.*;

public interface Shape {
	
	void draw(Graphics g);
	String getID();
}
