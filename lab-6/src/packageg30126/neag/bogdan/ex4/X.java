package packageg30126.neag.bogdan.ex4;
import java.lang.CharSequence;
import java.lang.String;
public class X implements CharSequence{
	
	
	private char[] sir;
	
	public X(char[] sir) {
		this.sir=sir;
	}
	@Override
	public char charAt(int i) {
		// TODO Auto-generated method stub
		if(i>-1 && i<sir.length)
			return sir[i];
		return 0;
	}

	@Override
	public int length() {
		// TODO Auto-generated method stub
		return sir.length;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return new String(sir);
	}
	
	@Override
	public CharSequence subSequence(int i, int j) {
		// TODO Auto-generated method stub
	char[] subsir = new char[j-i+1];
	int x=0;
	for(int y=i;y<=j;y++) {
		subsir[x++]=sir[y];
	}
	return new X(subsir);
	}
}
