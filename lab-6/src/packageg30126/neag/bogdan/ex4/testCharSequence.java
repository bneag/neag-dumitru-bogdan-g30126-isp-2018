package packageg30126.neag.bogdan.ex4;

import static org.junit.Assert.*;

import org.junit.Test;


public class testCharSequence {

	@Test
	public void test() {
		X sir = new X(new char[]{'a','b','c','d','0','z','e','A'});
		assertEquals('z',sir.charAt(5));
		assertEquals(8,sir.length());
		assertEquals("abcd0zeA",sir.toString());
		X sir2 = new X(new char[]{'y','x','c','d','0','w','l','A','X'});
		assertEquals("d0",sir2.subSequence(3, 4));
	}

	

}
