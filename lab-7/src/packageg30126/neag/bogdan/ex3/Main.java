package packageg30126.neag.bogdan.ex3;

import java.util.TreeSet;

import packageg30126.neag.bogdan.ex1.BankAccount;
import packageg30126.neag.bogdan.ex1.BankAccount;
public class Main {

	
	public static void main(String[] args) {
		
		
		Bank bank=new Bank();
		
		bank.addAccount("Dan", 510);
		bank.addAccount("Bogdan", 300);
		bank.addAccount("Alin", 200);
		bank.addAccount("Ion", 800);
		bank.addAccount("Ion", 300);
		bank.printAccounts();
		
		System.out.println("-------------");
		System.out.println(bank.getAllAccounts());

	}
}
