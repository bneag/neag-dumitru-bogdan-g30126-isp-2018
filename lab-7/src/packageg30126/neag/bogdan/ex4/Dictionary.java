package packageg30126.neag.bogdan.ex4;

import java.util.Collection;
import java.util.HashMap;

import javax.sound.midi.Soundbank;

public class Dictionary {
	HashMap<Word, Definition>hashMap=new HashMap<>();

	
	public void addWord(Word w,Definition d) {
		hashMap.put(w, d);
	}
	public Definition getDefinition(Word w)
	{
		return hashMap.get(w);
		
	}
	public void getAllWords() {
		Collection<Word> words = hashMap.keySet();
		System.out.println("Cuvintele din dictionat: ");
		for(Word x:words)
			System.out.println(x.getName());
	}
	public void getAllDefinitions() {
		Collection<Definition> def = hashMap.values();
		System.out.println("definitiile sunt: ");
		for(Definition x:def)
			System.out.println(x.getDefinition());
	}

}
