package packageg30126.neag.bogdan.ex4;



public class Word {
	
	private String name;

	public Word(String name) {
		this.name=name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	
}
