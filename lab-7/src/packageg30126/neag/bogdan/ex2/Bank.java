package packageg30126.neag.bogdan.ex2;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import packageg30126.neag.bogdan.ex1.BankAccount;

public class Bank {
	private ArrayList<BankAccount> accounts = new ArrayList<>();
	
	public void addAccount(String owner, double balance) {
		BankAccount b = new BankAccount(owner,balance);
        accounts.add(b);
	}
	
	public void printAccounts(){
		
		Collections.sort(accounts);
		 Iterator<BankAccount> i = accounts.iterator();
        while(i.hasNext()){
            BankAccount acc = i.next();
            System.out.println(acc);
     
        }
		
	}	

	public void printAccounts(double minBalance, double maxBalance){
		
        Iterator<BankAccount> i = accounts.iterator();
        while(i.hasNext()){
            BankAccount acc = i.next();
            if(acc.getBalance() > minBalance && acc.getBalance() < maxBalance)
            System.out.println(acc);
        	
        }
	}
public void printAccounts2(){
		
		//Collections.sort(accounts);
		 Iterator<BankAccount> i = accounts.iterator();
        while(i.hasNext()){
            BankAccount acc = i.next();
            System.out.println(acc);
     
        }
		
	}	
	public BankAccount getAccount(String owner){
    
	Iterator<BankAccount> i = accounts.iterator();
    while(i.hasNext()){
        BankAccount acc = i.next();
        if(acc.getOwner().equalsIgnoreCase(owner))
            return acc;
    }
    return null;
}

public ArrayList<BankAccount> getAllAccounts() {

	return accounts;
}

}
