package packageg30126.neag.bogdan.ex2;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import packageg30126.neag.bogdan.ex1.BankAccount;

public class TestMain {
	
	public static void main(String[] args) {
	Bank bn = new Bank();
	
	bn.addAccount("Bogdan",100);
	bn.addAccount("Alin",900);
	bn.addAccount("Dan",600);
	bn.addAccount("Marius",500);
	bn.addAccount("Ion",400);
	bn.addAccount("Mihai",300);
	bn.addAccount("Mircea",200);
	
	//bn.printAccounts();
	bn.getAccount("Bogdan");
	//intre 2 valori alea balance
	bn.printAccounts(300,600);
	System.out.println();
	//dupa balance
	System.out.println();
	bn.printAccounts();
	System.out.println();
	System.out.println();
	
	Collections.sort(bn.getAllAccounts(),BankAccount.bncomp);
	bn.printAccounts2();
	
	
	}

}
