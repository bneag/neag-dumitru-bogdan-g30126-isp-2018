package packageg30126.neag.bogdan.ex1;

import java.util.Comparator;

import packageg30126.neag.bogdan.ex2.Bank;

public class BankAccount implements Comparable<BankAccount>{
	private String owner;
	private double balance;
	
	public BankAccount(String owner,double balance) {
		this.owner=owner;
		this.balance=balance;
	}
	
	public void withdraw(double amount) {
		if(balance-amount>0) {
            balance -= amount;
            
        }
			
	}
	
	public void deposit(double amount) {
		balance += amount;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(balance);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BankAccount other = (BankAccount) obj;
		if (Double.doubleToLongBits(balance) != Double.doubleToLongBits(other.balance))
			return false;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner))
			return false;
		return true;
	}
	public String getOwner() {
		return owner;
	}
	public double getBalance() {
		return balance;
	}

	@Override
	public String toString() {
		return "BankAccount [owner=" + owner + ", balance=" + balance + "]\n";
	}

	public int compareTo(BankAccount compareBank) {
		int compareBalance = (int) ((BankAccount)compareBank).getBalance();
		return (int) (this.balance-compareBalance);
	}
	
	public static Comparator<BankAccount> bncomp = new Comparator<BankAccount>(){
		
		@Override
		public int compare(BankAccount b1, BankAccount b2) {
			String owner1 = b1.getOwner().toUpperCase();
			String owner2 = b2.getOwner().toUpperCase();
			return owner1.compareTo(owner2);
		}
};
}
