package packageg30126.neag.bogdan.ex1;

public class Cofee {
	
	 private int temp;
     private int conc;
    

     Cofee(int t,int c)
     {temp = t;conc = c;}
     
     int getTemp(){return temp;}
     int getConc(){return conc;}
	
     @Override
	public String toString() {
		return "Cofee [temp=" + temp + ", conc=" + conc + "]";
	}
     
}
