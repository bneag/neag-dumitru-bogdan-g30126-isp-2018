package packageg30126.neag.bogdan.ex1;

public class TooManyCofeeException extends Exception{
	
	private int count=0;
	
	public TooManyCofeeException(int count,String msg) {
		super(msg);
		this.count=count;
		
	}
	
	public int getCount() {
		return count;
	}
	

}
