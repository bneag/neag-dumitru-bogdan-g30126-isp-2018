package packageg30126.neag.bogdan.ex1;

public class CofeeMaker {
	
	int count;
	
	Cofee makeCofee() throws TooManyCofeeException{
        System.out.println("Make a coffe");
        int t = (int)(Math.random()*100);
        int c = (int)(Math.random()*100);
        
        Cofee cofee = new Cofee(t, c);
        count++;
        if(count>10)
        	throw new TooManyCofeeException(count,"Too many cofees");
        return cofee;
  }

}
