package packageg30126.neag.bogdan.reex6;

public class MyPoint {
	
	private int x,y;
	
	public MyPoint()
	{
		x=y=0;
		
	}
	
	public MyPoint(int x1,int y1)
	{
		this.x=x1;
		this.y=y1;
	}
	
	public int getX()
	{
		return x;
	}
	
	public int getY()
	{
		return y;
	}
	
	public void setX() {
		this.x=x;
	}
	
	public void setY() {
		this.y=y;
	}
	
	public void setXY(int x,int y)
	{
		this.x=x;
		this.y=y;
	}
	
	public String toString()
	{
		return "("+x+","+y+")";
	}
	
	public double distance(int x,int y) {
		double dist = Math.sqrt((this.x-x)*(this.x-x)+(this.y-y)*(this.y-y));
		return dist;
	}
	
	public double distance(MyPoint a) {
		return Math.sqrt((x-a.getX())*(x-a.getX())+(y-a.getY())*(y-a.getY()));
	}
	
}

