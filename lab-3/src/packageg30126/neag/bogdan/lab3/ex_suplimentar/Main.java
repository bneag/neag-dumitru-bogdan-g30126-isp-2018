package packageg30126.neag.bogdan.lab3.ex_suplimentar;

//import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class Main {

    public static int solution(int[] A){

        for(int i=0; i<A.length;i++)
        {
        	boolean unpaired = false;
        	for(int j=0;j<A.length;j++)
        	{
        		if(A[i]==A[j] && i!=j)
        		{
        			unpaired = true;
        		}
        	}
        	if(unpaired==false)
        		return A[i];
        }
        return -1;
    }

    public static void main(String[] args) {
        int[] A = new int[7];
        A[0] = 9;  A[1] = 3;  A[2] = 9;
        A[3] = 3;  A[4] = 9;  A[5] = 7;
        A[6] = 9;
        int result = solution(A);

        if(result==7)
            System.out.println("Rezultat corect.");
        else
            System.out.println("Rezultat incorect.");
    }
}
