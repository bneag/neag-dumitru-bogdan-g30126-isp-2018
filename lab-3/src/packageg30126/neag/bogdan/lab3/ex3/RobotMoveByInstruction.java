package packageg30126.neag.bogdan.lab3.ex3;

import becker.robots.*;


public class RobotMoveByInstruction {
	 public static void main(String[] args)
	   {  
	   	// Set up the initial situation
	   	City prague = new City();
	      Robot karel = new Robot(prague, 1, 1, Direction.NORTH);
	 
			// Direct the robot to the final situation
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.turnLeft();	// start turning right as three turn lefts
	      karel.turnLeft();
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.turnLeft();	// start turning right as three turn lefts
	      karel.turnLeft();
	   }
}
