package packageg30126.neag.bogdan.lab3.ex4;

import becker.robots.*;

public class Clockwise {
	
	public static void main(String[] args) {
		// Set up the initial situation
	      City ny = new City();
	      
	      Wall blockAve0 = new Wall(ny, 1, 1, Direction.WEST);//
	      Wall blockAve4 = new Wall(ny, 2, 1, Direction.WEST);//
	      Wall blockAve1 = new Wall(ny, 2, 2, Direction.SOUTH);
	      Wall blockAve5 = new Wall(ny, 2, 1, Direction.SOUTH);
	      Wall blockAve2 = new Wall(ny, 1, 2, Direction.EAST);//
	      Wall blockAve6 = new Wall(ny, 2, 2, Direction.EAST);//
	      Wall blockAve3 = new Wall(ny, 1, 2, Direction.NORTH);
	      Wall blockAve7 = new Wall(ny, 1, 1, Direction.NORTH);
	      
	      Robot mark = new Robot(ny, 0, 2, Direction.WEST);
	      
	   // mark goes around the roadblock
	      mark.move();
	      mark.move();
	      mark.turnLeft();     // start turning right as three turn lefts
	      mark.move();
	      mark.move();
	      mark.move();
	      mark.turnLeft();     // finished turning right
	      mark.move();
	      mark.move();
	      mark.move();
	      mark.turnLeft();     // finished turning right
	      mark.move();
	      mark.move();
	      mark.move();
	      mark.turnLeft();
	      mark.move();
	}
}
