package packageg30126.neag.bogdan.lab3.ex5;
import becker.robots.*;

public class NewsPaper {
	
	public static void main(String[] args) {
		// Set up the initial situation
	      City ny = new City();
	      Thing parcel = new Thing(ny, 2, 2);
	      
	      Wall blockAve0 = new Wall(ny, 1, 1, Direction.WEST);//
	      Wall blockAve4 = new Wall(ny, 2, 1, Direction.WEST);//
	      Wall blockAve1 = new Wall(ny, 1, 2, Direction.SOUTH);
	      Wall blockAve5 = new Wall(ny, 2, 1, Direction.SOUTH);
	      Wall blockAve2 = new Wall(ny, 1, 2, Direction.EAST);//
	      //Wall blockAve6 = new Wall(ny, 2, 2, Direction.EAST);//
	      Wall blockAve3 = new Wall(ny, 1, 2, Direction.NORTH);
	      Wall blockAve7 = new Wall(ny, 1, 1, Direction.NORTH);
	      
	      Robot karel = new Robot(ny, 1, 2, Direction.SOUTH);
	      
	   // karel goes after the newspaper
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.move();
	      
	      karel.turnLeft();
	      karel.move();
	      
	      karel.turnLeft();
	      karel.move();
	      karel.pickThing();
	      
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.move();
	      
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.move();
	      
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.move();
	      
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.putThing();
	    
}
}
