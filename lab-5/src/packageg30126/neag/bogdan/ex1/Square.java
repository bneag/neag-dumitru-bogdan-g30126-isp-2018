package packageg30126.neag.bogdan.ex1;

public class Square extends Rectangle {
public Square() {
		
	}
	 
	public Square(double side) {
		super(side,side);
	}
	public Square(double side,String color,boolean filled) {
		super(side,side,color,filled);
	}
	
	public double getSide() {
		return getWidth();
	}
	public void setSide(double side) {
		setWidth(side);
	}
	
	public void setWidth(double side) {
		setWidth(side);
	}
	public void setLength(double side) {
		setLength(side);
	}
	public String toString() {
		super.toString();
		return "A Square with side="+getSide()+", which is a subclass of"+ super.toString();
	}

}
