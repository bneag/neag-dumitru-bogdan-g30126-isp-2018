package packageg30126.neag.bogdan.ex1;

public abstract class Shape {
	
	protected String color;
	protected boolean filled;
	
	public Shape() {
		
	}    
	public Shape(String color, boolean filled) {

	       this.color = color;
	       this.filled = filled;
	   }
	   public String getColor() {

	       return color;

	   }    
	   public void setColor(String color) {
	       this.color = color;

	   }
	public boolean isFilled() {
       return filled;

	   }
	public void setFilled(boolean filled) {
			this.filled = filled;

	   }
	 public abstract double getArea();   
	 public abstract double getPerimeter(); 
	 @Override
	 public String toString() {

	       return "Shape{" +
	    		   "color='" + color + '\'' +
	               ", filled=" + filled +
	               '}';

	   }
	 
	 public static void main(String[] args) {
		 Shape[] sh = new Shape[3];
			
			
			sh[0]=new Circle("blue",true,2.0);
			sh[1]=new Rectangle(2.0,3.0);
			sh[2]=new Square(2.0);
			
			for(int i=0;i<3;i++) {
				System.out.println("Aria "+sh[i].getArea());
				System.out.println("Perimetru "+sh[i].getPerimeter());
				
			}
	 }

	}

