package packageg30126.neag.bogdan.ex2;

public class ProxyImage implements Image {
	private RealImage realImage;
	private RotateImage rotateImage;
	   private String fileName;
	 
	   public ProxyImage(String fileName){
	      this.fileName = fileName;
	   }
	 
	   @Override
	   public void display() {
	      if(realImage == null ||  rotateImage==null){
	         realImage = new RealImage(fileName);
	         rotateImage = new  RotateImage(fileName);
	      }
	      realImage.display();
	      rotateImage.display();
	   }
	   
}
