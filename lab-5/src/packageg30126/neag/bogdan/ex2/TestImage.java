package packageg30126.neag.bogdan.ex2;

public class TestImage  {
	
	public static void main(String[] args) {
		RealImage rm  = new RealImage("zeu");
		rm.display();
		
		ProxyImage pm = new ProxyImage("rac");
		pm.display();
		
		RotateImage ro = new RotateImage("nota");
		ro.display();
	}
}
