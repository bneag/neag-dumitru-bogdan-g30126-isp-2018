package packageg30126.neag.bogdan.ex3;

public class Controller {
	
	TemperatureSensor tempSensor;
	LightSensor lightSensor;
	
	public static void control() {
		
		 Sensor ts = new TemperatureSensor();
		 Sensor ls = new LightSensor();
		
		 int i=0;
		 int sec=0;
		for(i=0;i<20;i++) {
			try {
			Thread.sleep(1000);
			sec++;
			System.out.println((i+1)+" read value");
			System.out.println("Value readed for ts is: "+ts.readValue());
			System.out.println("Value readed for ls is: "+ls.readValue());
			System.out.println();
			}catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
		public static void main(String[]args) {
			control();
		}
	}


