package packageg30126.neag.bogdan.lab2.ex4;

import java.util.Random;
import java.util.Scanner;

public class ArrayMaxim {
	
	static int max(int a,int b){
		if(a>b)
			return a;
		else return b;
	}
	
	public static void main(String[] args){
		
		int i,n;
		int[] x = null;
		Random r = new Random();
		
		Scanner in = new Scanner(System.in);
		System.out.println("Dati numarul de elemente al vectorului n=");
		n = in.nextInt();
		in.close();
		x = new int[n];
		
		for(i=0; i<n; i++)
			{
			x[i] = r.nextInt(); 
			System.out.println(+x[i]);
			}
		
		int maxim = x[0];
		
		for(i=0; i<n; i++)
			if(max(x[i],maxim)==x[i])
				maxim=max(x[i],maxim);
		System.out.println("Maximul din vector este" +maxim);
	}
}
