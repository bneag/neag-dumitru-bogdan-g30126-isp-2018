package packageg30126.neag.bogdan.lab2.ex7;

import java.util.Random;
import java.util.Scanner;

public class GuessNo {
	
	
	public static void main(String[] args){
		
		int x,n,i;
		
		Random r = new Random();
		x = r.nextInt(10);
	
		i=1;
		Scanner in = new Scanner(System.in);
		System.out.println("Guess the number ");
		
		System.out.printf("\n%d attemp\n",i);
		n = in.nextInt();
		
		while(n!=x && i!=3){
			
			
			if(n>x){
			System.out.println("Wrong answer, your number it too high");
			}
			else  if(n<x) {
				System.out.println("Wrong answer, your number it too low");
				
			}
			i++;
			System.out.printf("\n%d attemp\n",i);
			n = in.nextInt();
		}
		
		if(i==3 &&n!=x){
			System.out.println("You lost!");
			System.out.println("The number was: "+x);
		}
		
		else if(n==x) {
			System.out.println("You Win");
			
		}
		
		in.close();
	}
}
