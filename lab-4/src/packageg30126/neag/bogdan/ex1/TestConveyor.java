package packageg30126.neag.bogdan.ex1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author mihai.hulea
 */
public class TestConveyor {

    @Test
    public void shouldAddBox(){
        Conveyor c = new Conveyor();
        Box b = new Box(c,0,1);
        assertEquals(b.getId(), c.getBox(0).getId());
    }

    @Test
    public void shouldMoveBoxToRight(){
        Conveyor c = new Conveyor();
        Box b = new Box(c,1,1);
        c.moveRight();
        assertEquals(b.getId(), c.getBox(2).getId());
    }

    @Test
    public void shouldMoveBoxToLeft(){
        Conveyor c = new Conveyor();
        Box b = new Box(c,1,1);
        c.moveLeft();
        assertEquals(b.getId(), c.getBox(0).getId());

    }
    
    @Test
    public void shouldRemoveaBox(){
        Conveyor c = new Conveyor();
        Box b = new Box(c,0,1);
        c.pickBox(0);
        assertEquals(null, c.getBox(1));

    }

}