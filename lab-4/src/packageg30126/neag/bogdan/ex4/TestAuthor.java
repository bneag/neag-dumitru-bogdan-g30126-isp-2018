package packageg30126.neag.bogdan.ex4;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestAuthor {
	@Test
	public void shouldPrintString() {
		Author a1 = new Author("a1","a1@email.com",'m');
		assertEquals("Author a1 ( m ) at email a1@email.com",a1.toString());
	}
}
