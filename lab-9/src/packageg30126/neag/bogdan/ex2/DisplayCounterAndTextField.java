package packageg30126.neag.bogdan.ex2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class DisplayCounterAndTextField extends JFrame implements ActionListener{
	
	
	JLabel counter;
	JTextField tCounter;
	
	JButton bCount;
	private int c=0;
	
	DisplayCounterAndTextField(){
		setTitle("Buton Counter");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,300);
        setVisible(true);
	}
	public void init(){
		 
        this.setLayout(null);
        int width=80;int height = 20;

        counter = new JLabel("Counter ");
        counter.setBounds(10, 50, width, height);

        

        tCounter = new JTextField();
        tCounter.setBounds(70,50,width, height);

        

        bCount = new JButton("Count");
        bCount.setBounds(10,150,width, height);

        
        bCount.addActionListener(this);
        
        add(counter);add(tCounter);add(bCount);
        

  }
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		c++;
		tCounter.setText(c+"");
		
	}
	public static void main(String[] args) {
		new  DisplayCounterAndTextField();
	}
}
