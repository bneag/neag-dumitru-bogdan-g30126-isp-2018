package packageg30126.neag.bogdan.ex4;

import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

public class XO extends JFrame implements ActionListener{
	
	JButton[] buttons = new JButton[9];
	int count;
	private String letter;
	
	XO(){
		setTitle("Tic Tac Toe");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		init();
		setSize(600,600);
		setLayout(new GridLayout(3,3));
		setVisible(true);
		
	}
	
	void init() {
		count=0;
		for(int i=0;i<9;i++) {
			buttons[i]=new JButton("");
			buttons[i].addActionListener(this);
			add(buttons[i]);
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		count++;
		ImageIcon X = new ImageIcon("X.PNG");
		ImageIcon O = new ImageIcon("O.PNG");
		
		if(count%2==0)
			letter="X";
		else letter="O";
		
		for(JButton jb:buttons) {
			if(e.getSource()==jb) {
				switch(letter) {
				case "X":
					jb.setText(letter);
					jb.setEnabled(false);
					break;
				
				case "O":
					jb.setText(letter);
					jb.setEnabled(false);
					break;
				}
			}
		}
		
		
	}
	
	public static void main(String[] args) {
		 new XO();
	}
	
}