package packageg30126.neag.bogdan.ex3;

public class Counter extends Thread {
	
	private String name;
	private Thread t;
	private int n1,n2;
	
 Counter(String name, int n1,int n2, Thread t){
		this.name=name;
		this.n1=n1;
		this.n2=n2;
		this.t=t;
	}
 
 public void run() {
	 System.out.println(name+"started it`s process");
	 
	 try {
		 if(t!=null)
			 t.join();
		 System.out.println(name+" executing counts");
		 
		 for(int i=n1;i<=n2;i++) {
			 System.out.println(i);
			 Thread.sleep(100);
		 }
		
	 }catch(Exception e) {
		 e.printStackTrace();
	 }
 }
 
 public static void main(String[] args) {
	 Counter c1 = new Counter("C1",0,100,null);
	 Counter c2 = new Counter("C2",100,200,c1);
	 
	 c1.start();
	 c2.start();
 }
}
