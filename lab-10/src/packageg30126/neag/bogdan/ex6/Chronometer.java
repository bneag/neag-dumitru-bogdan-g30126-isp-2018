package packageg30126.neag.bogdan.ex6;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Chronometer extends JFrame{

	JLabel tl1 , tl2, tl3;
	JButton start , reset , stop;
	
	Timer t;
	
	Chronometer(){
		setTitle("Timer");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		init();
		setSize(500,250);
		setVisible(true);
	}
	
	public void init(){
		
		this.setLayout(null);
		
		tl1 = new JLabel(String.format("%02d", 0) + " : ");
		tl1.setBounds(180, 70, 50, 17);
		tl1.setFont(new Font("" , Font.BOLD , 24));
		add(tl1);
		
		tl2 = new JLabel(String.format("%02d",0) + " : ");
		tl2.setBounds(230, 70, 50, 17);
		tl2.setFont(new Font("" , Font.BOLD , 24));
		add(tl2);
		
		tl3 = new JLabel(String.format("%02d", 0));
		tl3.setBounds(280, 70, 50, 17);
		tl3.setFont(new Font("" , Font.BOLD , 24));
		add(tl3);
		
		start = new JButton("Start");
		start.setBounds(130, 120, 100, 30);
		add(start);
		start.setContentAreaFilled(false);
		start.addActionListener(new ButtonActionListener());
		
		
		reset = new JButton("Reset");
		reset.setBounds(257, 120, 100, 30);
		add(reset);
		reset.setContentAreaFilled(false);
		reset.addActionListener(new ButtonActionListener());
		
		t= new Timer(this);
	}
	
	class ButtonActionListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			switch(e.getActionCommand()){
			case "Start":
				start.setText("Pause");
				
				if(!t.isAlive())
					t.start();
				t.starting();
				
				break;
				
			case "Pause" :
				start.setText("Start");
				t.pause();
				
				break;
				
			case "Reset" :
				t.pause();
				t.min = 0;
				t.sec = 0;
				t.msec = 0;
				
				start.setText("Start");
				
				tl1.setText(String.format("%02d",0) + " : ");
				tl2.setText(String.format("%02d",0) + " : ");
				tl3.setText("00");
				break;
				
			}
		}
		
	}
	
	public static void main(String[] args){
		new Chronometer();
	}
	
}
