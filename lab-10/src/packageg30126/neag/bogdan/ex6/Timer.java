package packageg30126.neag.bogdan.ex6;

public class Timer extends Thread{
	
	int sec , min , msec;
	boolean paused;
	Chronometer c;
	
	Timer(Chronometer c){
		this.c = c;
		
	}
	
	public void run(){
		while(true){
			msec+=1;
			
			
			
			if(msec<100)
				c.tl3.setText(String.format("%02d", msec));
			else if((msec >= 100) && (sec < 60)){
				msec =0;
				sec++;
				c.tl2.setText(String.format("%02d", sec) + " : ");
				c.tl3.setText(String.format("%02d", msec));
			}
			else if((msec >1000) && (sec >=60)){
				msec = 0;
				sec = 0;
				min++;
				c.tl1.setText(String.format("%02d", min) + " : ");
				c.tl2.setText(String.format("%02d", sec) + " : ");
				c.tl3.setText(String.format("%02d", msec));
			}
			
			synchronized(this){
				if(paused){
					try{
						wait();
					}catch(Exception e){}
					
				}
			}
			
			try{
				Thread.sleep(10);
			}catch(Exception e){}
			
		}
	}
	
	public synchronized void pause(){
		paused = true;
	}
	
	public synchronized void starting(){
		paused = false;
		this.notify();
	}

}
