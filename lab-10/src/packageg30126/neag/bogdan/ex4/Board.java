package packageg30126.neag.bogdan.ex4;

import java.util.ArrayList;

public class Board  {
	public ArrayList<Robot> robot = new ArrayList<>();Thread t =new Thread();
	public void addRobot(Robot r) {
		robot.add(r);
	}
	public void removeRobot(Robot r) {
		robot.remove(r);
	}
	public void showRobots() {
		for(Robot r2 : robot) {
			if(r2!=null)
			System.out.println(r2.getNameRobot()+" ");
		}
	}
	public void moveRandom (Robot r) {
		int x;
		x=(int)(Math.random()*100);
		if(x>=0 && x<=25 && r.getX()<99) {
			r.setX(r.getX()+1);
		}
		else if(x>=0 && x<=25 && r.getX()==99) {
			r.setX(r.getX()-1);
		}
		else if(x>25 && x<=50 && r.getX()>0) {
			r.setX(r.getX()-1);
		}
		else if(x>25 && x<=50 && r.getX()==0)
			r.setX(r.getX()+1);
		else if(x>50 && x<=75 && r.getY()<99)
			r.setY(r.getY()+1);
		else if(x>50 && x<=75 && r.getY()==99) {
			r.setY(r.getY()-1);
		}
		else if(x>75 && x<=100 && r.getY()>0) {
			r.setY(r.getY()-1);
		}
		else if(x>75 && x<=100 && r.getY()==0) {
			r.setY(r.getY()+1);
		}
	}
	public void checkPositions(Robot r1 ) {
		for(Robot r2 : robot) {
			if(!(r1.getNameRobot().equals(r2.getNameRobot())) && r1.getX()==r2.getX() && r1.getY()==r2.getY()) {
				r1.setAlive(false);
				r2.setAlive(false);
			}	
		}

	}
}