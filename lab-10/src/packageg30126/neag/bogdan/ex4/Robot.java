package packageg30126.neag.bogdan.ex4;
public class Robot extends Thread {
	int x;
	int y;
	String name;
	boolean alive;
	static Board b = new Board();
	public Robot(String name, int x, int y) {
		this.name=name;
		this.x=x;
		this.y=y;
		alive=true;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public String getNameRobot() {
		return name;
	}
	public void setNameRobot(String name) {
		this.name = name;
	}
	public boolean isAliveRobot() {
		return alive;
	}
	public void setAlive(boolean alive) {
		this.alive = alive;
	}
	@Override
	public void run() {
		
		while(true  ) {	
			try {	if(! isAliveRobot())
			{b.removeRobot(this);
			System.out.println(this.getNameRobot()+ " is dead!");
			return;}
					if(b.robot.contains(this) ) {
					b.moveRandom(this);
					b.checkPositions(this);
					System.out.println("Robot "+ this.getNameRobot() + " is at position X: "+this.getX()+" and Y: "+this.getY()+" and is still alive: "+this.isAliveRobot());
					if(this.isAliveRobot()==false) {
						b.removeRobot(this);
						System.out.println(this.getNameRobot()+ " is dead!");
						return;
					}
					}	
				Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		}
	}
	public static void main(String[] args) {
		Robot r0 = new Robot("R0",1,1);
		Robot r1 = new Robot("R1",1,1);
		Robot r2 = new Robot("R2",2,2);
		Robot r3 = new Robot("R3",3,3);
		Robot r4 = new Robot("R4",4,4);
		Robot r5 = new Robot("R5",5,5);
		Robot r6 = new Robot("R6",6,6);
		Robot r7 = new Robot("R7",7,7);
		Robot r8 = new Robot("R8",8,8);
		Robot r9 = new Robot("R9",2,1);
		b.addRobot(r0);
		b.addRobot(r1);
		b.addRobot(r2);
		b.addRobot(r3);
		b.addRobot(r4);
		b.addRobot(r5);
		b.addRobot(r6);
		b.addRobot(r7);
		b.addRobot(r8);
		b.addRobot(r9);
		r0.start();
		r1.start();
		r2.start();
		r3.start();
		r4.start();
		r5.start();
		r6.start();
		r7.start();
		r8.start();
		r9.start();
		Thread t = new Thread ();
		try {
			t.sleep(10000);
			b.showRobots();
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
	}
}
